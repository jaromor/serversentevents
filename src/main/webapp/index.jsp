<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<title></title>

		<script type="text/javascript">
			function connect() {
				var es = new EventSource('sse');
				es.onmessage = function(event) {
						console.log(event.data);
					};
				es.addEventListener('close', function(event) {
						console.log(event.data);
						this.close();
					});
				es.onerror = function(event) {
						console.log(es.readyState);
						if(es.readyState === EventSource.CLOSED) {
							if(confirm('[EventSource] Error while connecting to ' + this.url + '\nClose?'))
								es.close(); // pretty much a no-op at this point
						}
					};
			}
		</script>
	</head>

	<body>
		<button onclick="connect()">Connect</button>
	</body>
</html>
