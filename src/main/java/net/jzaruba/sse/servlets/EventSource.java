package net.jzaruba.sse.servlets;

import javax.servlet.AsyncContext;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@WebServlet(urlPatterns = {"/sse"}, asyncSupported = true)
public class EventSource extends HttpServlet {
    private static final long serialVersionUID = -3886345608385486198L;

    private static ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/event-stream");
        response.setHeader("Connection", "close");
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Transfer-Encoding", "identity");

        final AsyncContext asyncContext = request.startAsync();

        Runnable runnableDummy = new Runnable() {
            @Override
            public void run() {
                try {
                    ServletResponse response = asyncContext.getResponse();
                    PrintWriter writer = response.getWriter();

                    // hold the connection for 3 seconds in total
                    for (int i = 0; i < 3; i++) {
                        Thread.sleep(1000);
                        // say Hello in the meantime
                        writer.println("data: Hello " + i);
                        writer.println();
                        writer.flush();
                    }

                    // in case we don't wanna talk to the client anymore
                    signalConnectionCloseToClient(writer);

                    // after three seconds close
                    asyncContext.complete();
                } catch (IOException | InterruptedException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
            }
        };

        executor.execute(runnableDummy);
    }

    private void signalConnectionCloseToClient(PrintWriter writer) {
        writer.println("event: close");
        writer.println("data: you were a bad girl");
        writer.println();
        writer.flush();
    }

    @Override
    public void destroy() {
        executor.shutdown();
    }
}
